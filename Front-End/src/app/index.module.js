(function() {
  'use strict';

  angular
    .module('copySports', ['ui.bootstrap.datetimepicker', 'ksSwiper', 'ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages',
                            'angularMoment', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr']);

})();
