(function() {
  'use strict';

  angular
    .module('copySports')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope, $state) {

    $rootScope.$on('$stateChangeError', function() {
      $state.go('login');
    })

    // $rootScope.$on('$stateChangeStart', function () {
    //
    // })

    $log.debug('runBlock end');
  }

})();
