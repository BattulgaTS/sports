(function() {
  'use strict';

  angular
    .module('copySports')
    .controller('newTeamController', newTeamController);

  function newTeamController(server, $log) {
    var vm = this;
    vm.team = {};

    vm.Send = function() {
      $log.debug('sending new team info')

      

      server.post('/newteam', vm.team, function(err, data){
        if (err) $log.debug(err);
        if (data){
          vm.team = {};
          //vm.getPost();
        }
      });
    };

  }
})();
