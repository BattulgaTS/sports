(function() {
  'use strict';

  angular
    .module('copySports')
    .controller('matchResultController', matchResultController);

  function matchResultController(server, $log) {
    var vm = this;
    vm.match = {};

    vm.Send = function() {
      $log.debug('sending new match info ',vm.match)
      server.post('/new/match', vm.match, function(err, data){
        if (err) $log.debug(err);
        if (data === 'success!'){
          vm.match = {};
        }
      });
    };

    vm.teams = [];

    vm.GetTeams = function(){
      var vm = this;
      vm.req = {};

      server.req('/get/teams', vm.req, 'GET').then(function(res){
        vm.teamas = res;
      }, function(err){
        $log.debug(err);
      })

    }
    vm.GetTeams();
  }
})();
