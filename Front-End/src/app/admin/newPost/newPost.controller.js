(function() {
  'use strict';

  angular
    .module('copySports')
    .controller('newPostController', newPostController);

  function newPostController(server, $log) {
    var vm = this;
    vm.newPost = {};
    vm.newPost.Top = false;
    vm.posts = {};

    vm.Submit = function() {
      server.req('/newpost', vm.newPost, 'POST').then(function(res){
        if (res){
          vm.newPost = {};
          vm.newPost.Top = false;
          vm.getPost();
        }
      }, function(err){
        $log.error(err);
      })
    };

    vm.getPost = function() {
      vm.req = {}
      server.req('/allposts', vm.req, 'GET').then(function(res){
        vm.posts = res;
      }, function(err){
        $log.debug(err)
      })
    }

    vm.getPost();
  }
})();
