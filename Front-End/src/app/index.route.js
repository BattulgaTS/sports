(function() {
  'use strict';

  angular
    .module('copySports')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider

      // HOME
      .state('home', {
        url: '',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })

      .state('home.withslider', {
        url: '',
        templateUrl: 'app/components/swiper/slider.html',
        controller: 'SwiperController',
        controllerAs: 'swiper'
      })

      .state('home.withslider.posts', {
        url: '/',
        templateUrl: 'app/components/posts/posts.html',
        controller: 'homePageController',
        controllerAs: 'home'
      })

      .state('home.withslider.category', {
        url: '/category/:ct',
        templateUrl: 'app/components/posts/category.html',
        controller: 'categoryController',
        controllerAs: 'cate'
      })

      .state('home.teams', {
        url: '/teams',
        templateUrl: 'app/components/teams/teams.html',
        controller: 'TeamsController',
        controllerAs: 'teamctrl'
      })

      .state('home.withslider.singlePost',{
        url: '/post/:id',
        templateUrl: 'app/components/posts/singlepost.html',
        controller: 'SinglePostController',
        controllerAs: 'single'
      })

      .state('home.singleMatch', {
        url: '/match/:id',
        templateUrl: 'app/components/match/single/match.html',
        controller: 'singleMatchController',
        controllerAs: 'sMatchctrl'
      })

      //ADMIN

      .state('admin', {
        url: '/admin',
        templateUrl: 'app/admin/admin.html',
        resolve: {
          loggedIn: onlyLoggedIn
        }
      })

      .state('admin.newPost', {
        url: '/post',
        templateUrl: 'app/admin/newPost/newPost.html',
        controller: 'newPostController',
        controllerAs: 'new'
      })

      .state('admin.newTeam', {
        url: '/team',
        templateUrl: 'app/admin/newTeam/newTeam.html',
        controller: 'newTeamController',
        controllerAs: 'teamctrl'
      })

      .state('admin.newMatch', {
        url: '/match/new',
        templateUrl: 'app/admin/match/new.html',
        controller: 'matchResultController',
        controllerAs: 'MRctrl'
      })

      .state('admin.match.result', {
        url: '/match/report',
        templateUrl: 'app/admin/match/result.html',
        controller: 'newMatchController',
        controllerAs: 'matchctrl'
      })

      // USER

      .state('login', {
        url: '/login',
        templateUrl: 'app/components/login/login.html',
        controller: 'UserLoginController',
        controllerAs: 'login'
      })

      .state('register', {
        url: '/register',
        templateUrl: 'app/components/register/register.html',
        controller: 'RegisterController',
        controllerAs: 'reg'
      })

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
  }




  var onlyLoggedIn = function ($q, server, $log) {
    return true;
    // var deferred = $q.defer();
    // var vm = this;
    // vm.req = {};
    // server.req('/user/status', vm.req, 'GET').then(function(res){
    //   $log.debug(res);
    //   deferred.resolve();
    // }, function(err){
    //   $log.error(err);
    //   deferred.reject();
    // })
    //
    // return deferred.promise;
  };

})();
