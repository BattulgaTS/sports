/* global malarkey:false, moment:false */
(function() {
  'use strict';

  var config = {
    _APP_HOST: 'http://52.25.241.31',
    _APP_PORT: ':80/api',
    _DEV_HOST: 'http://localhost',
    _DEV_PORT: ':8080/api',
    _DEVELOPMENT: true
  };

  angular
    .module('copySports')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('config', config);

})();
