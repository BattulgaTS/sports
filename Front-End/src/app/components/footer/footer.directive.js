(function() {
  'use strict';

  angular
    .module('copySports')
    .directive('footerBottom', footerBottom);

  /** @ngInject */
  function footerBottom() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/footer/footer.html'
    };

    return directive;
  }

})();
