(function() {
  'use strict';

  angular
    .module('copySports')
    .controller('RegisterController', RegisterController);

  function RegisterController(server, $log, $state) {
    var vm = this;
    vm.newUser = {};
    vm.createIt = function () {
      $log.debug("createIt Function Triggered!");

      server.request('/register', vm.newUser, 'POST', function(err, data){
        if (err) $log.debug(err);
        if (data){
          vm.newUser = {};
          $state.go('home');
        }
      })
    };
  }

})();
