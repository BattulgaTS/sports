(function() {
  'use strict';

  angular
    .module('copySports')
    .controller('SwiperController', SwiperController);

  function SwiperController(server, $log) {
    var vm = this;
    vm.req = {};

    server.req('/topposts', vm.req, 'GET').then(function(res){
      vm.posts = res;
    }, function(err){
      $log.debug(err);
    })
  }

})();
