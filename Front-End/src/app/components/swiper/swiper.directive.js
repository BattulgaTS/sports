(function() {
  'use strict';

  angular
    .module('copySports')
    .directive('swiper', swiper);

  /** @ngInject */
  function swiper() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/swiper/swiper.html'
    };

    return directive;
  }

})();
