(function() {
  'use strict';

  angular
    .module('copySports')
    .directive('matches', matches);

  /** @ngInject */
  function matches() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/match/upcomingList/match.html',
      controller: MatchController,
      controllerAs: 'matchctrl'
    };

    return directive;

    /** @ngInject */
    function MatchController(server, $log, moment) {
      var vm = this;

      vm.matches = [];

      vm.calculateDiff = function(givenTime){
        return moment(givenTime).fromNow(true)
      }

      var req = {};

      server.req('/get/matches', req, 'GET').then(function(res){
        vm.matches = res;
      }, function(err){
        $log.debug(err);
      })

      //
      // server.request('/get/matches', {}, 'GET', function(err, data){
      //   if (err) $log.debug(err);
      //   if (data){
      //
      //     vm.matches = data;
      //     angular.forEach(vm.matches, function(value, index){
      //       if (value.team1_id)
      //         server.request('/get/team/' + value.team1_id, {}, 'GET', function(err, res){
      //           if (err) $log.debug(err);
      //           if (res) vm.matches[index].team1title = res[0].title;
      //         })
      //       else{
      //         $log.debug('team1 id is gone ',value.team1_id)
      //       }
      //
      //       if (value.team2_id)
      //         server.request('/get/team/' + value.team2_id, {}, 'GET', function(err, res){
      //           if (err) $log.debug(err);
      //           if (res) vm.matches[index].team2title = res[0].title;
      //         })
      //       else {
      //         $log.debug('team2 id is gone ', value.team2_id)
      //       }
      //     })
      //
      //   }
      // })
    }
  }

})();
