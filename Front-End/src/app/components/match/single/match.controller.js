(function(){
  angular
    .module('copySports')
    .controller('singleMatchController', singleMatchController)


  function singleMatchController(server, $log, $stateParams){
    var vm = this;
     server.request('/get/match/' + $stateParams.id, {}, 'GET',  function (err, res) {
       if (err) $log.debug(err);
       if (res){
         vm.match = res
         server.request('/get/team/' + vm.match[0].team1_id, {}, 'GET', function(er, re){
           if (er) $log.debug(er);
           if (re){
             vm.match[0].team1 = re[0];
           }
         });

         //var req = {};

        //  server.reqest('get/team/'+vm.match[0].team2_id, req, 'GET').then(function(res){
         //
        //  }, function(err){
         //
        //  })

         server.request('/get/team/' + vm.match[0].team2_id, {}, 'GET', function(er, re){
           if (er) $log.debug(er);
           if (re){
             vm.match[0].team2 = re[0];
           }
         });

       }
    });
  }

})();
