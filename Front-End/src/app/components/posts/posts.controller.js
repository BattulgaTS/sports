(function(){
  angular
    .module('copySports')
    .controller('homePageController', homePageController)


  function homePageController(server, $log){
    var vm = this;
    vm.req = {}

    server.req ('/allposts', vm.req, 'GET').then(function(res){
      vm.posts = res;
    }, function(err){
      $log.error(err);
    })
  }

})();
