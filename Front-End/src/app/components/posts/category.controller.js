(function(){
  angular
    .module('copySports')
    .controller('categoryController', categoryController)

  function categoryController(server, $stateParams, $log){
    var vm = this;
    vm.req = {};

    server.req('/category/' + $stateParams.ct, vm.req, 'GET').then(function(res){
      vm.posts = res;
    }, function(err){
      $log.error(err);
    });
  }

})();
