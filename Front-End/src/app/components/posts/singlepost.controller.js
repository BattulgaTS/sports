(function(){
  angular
    .module('copySports')
    .controller('SinglePostController', SinglePostController)


  function SinglePostController(server, $log, $stateParams){
    var vm = this;
    vm.req = {};

      server.req('/post/' + $stateParams.id, vm.req, 'GET').then(function(res){
        vm.post = res;
      }, function(err){
        $log.error(err);
      });
  }

})();
