(function() {
  'use strict';

  angular
    .module('copySports')
    .controller('TeamsController',  TeamsController);

  function TeamsController (server, $log) {
    var vm = this;
    vm.zone = '';
    vm.teams = [];

    vm.filterChange = function (newZone){
      vm.zone = newZone;
    };

    server.request('/get/teams', {}, 'GET', function(err, data){
      if (err) $log.debug(err);
      if (data){
        vm.teams = data;
      }
    })
  }
})();
