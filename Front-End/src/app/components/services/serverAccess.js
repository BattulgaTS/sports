(function() {
  'use strict';

  angular
      .module('copySports')
      .service('server', server);

  /** @ngInject */
  function server($http, $log, config, $q) {
    var vm = this;
    var hostUrl = '';

    if (config._DEVELOPMENT)  hostUrl += config._DEV_HOST + config._DEV_PORT;
    else                      hostUrl += config._APP_HOST + config._APP_PORT;


    var deferred = $q.defer()

    vm.req = function (route, data, method) {
      $log.debug('%s request %s',method , route);
      if (!method) method = 'GET';
      $http({
        url: hostUrl + route,
        method: method,
        withCredentials: true,
        data: data,
        cache: false
      })
      .success(function(data){
        $log.debug('data on SERVICE', data, method, route);
        deferred.resolve(data);
      })
      .error(function(err){
        deferred.reject(err)
      });

      return deferred.promise;
    }

    vm.post = function(uploadUrl, data, cb){
      var fd = new FormData();

      for(var key in data){
        fd.append(key, data[key]);
      }

      $http.post(hostUrl + uploadUrl, fd, {
        transformRequest: angular.identity,
        headers: { 'Content-Type': undefined }
      })

      .success(function(data){
        cb(null, data);
      })

      .error(function(err){
        cb(err);
      })

    }

    return {
      post: vm.post,
      req: vm.req
    }
  }

})();
