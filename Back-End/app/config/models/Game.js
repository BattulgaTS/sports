module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Game', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    winner_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    loser_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    match_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  // }, {
  //   instanceMethods: {
  //     countTasks: function() {
  //       // how to implement this method ?
  //     }
  //   }
  });
};
