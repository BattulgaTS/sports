module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Prize', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    amount: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    percent: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    tournament_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  // }, {
  //   instanceMethods: {
  //     countTasks: function() {
  //       // how to implement this method ?
  //     }
  //   }
  });
};
