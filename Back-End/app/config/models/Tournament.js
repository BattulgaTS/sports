module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Tournament', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    prizePool: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    sDate: {
      type: DataTypes.DATE,
      defaultValue: null
    },
    eDate: {
      type: DataTypes.DATE,
      defaultValue: null
    },
    tPrice: { // TicketPrice
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    cPrice: { // CompendiumPrice
      type: DataTypes.INTEGER,
      defaultValue: null
    }
  // }, {
  //   instanceMethods: {
  //     countTasks: function() {
  //       // how to implement this method ?
  //     }
  //   }
  });
};
