module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Participant', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    team_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    tournament_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    qualifiedT_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  // }, {
  //   instanceMethods: {
  //     countTasks: function() {
  //       // how to implement this method ?
  //     }
  //   }
  });
};
