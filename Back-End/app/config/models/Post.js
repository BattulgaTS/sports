module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Post', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    topic: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    category: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    content: {
      type: DataTypes.TEXT,
      defaultValue: null
    },
    picture: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    author: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    date: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    top: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }

  // }, {
  //   instanceMethods: {
  //     countTasks: function() {
  //       // how to implement this method ?
  //     }
  //   }
  });
};
