module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Draft', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    heroName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    priority: {
      type: DataTypes.INTEGER
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false
    },
    game_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  // }, {
  //   instanceMethods: {
  //     countTasks: function() {
  //       // how to implement this method ?
  //     }
  //   }
  });
};
