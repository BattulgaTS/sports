module.exports = function(sequelize, DataTypes) {
  return sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    type: {
      type: DataTypes.STRING,
      defaultValue: 'user'
    },
    username: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    password: {
      type: DataTypes.STRING,
      defaultValue: null
    }



  // }, {
  //   instanceMethods: {
  //     countTasks: function() {
  //       // how to implement this method ?
  //     }
  //   }
  });
};
