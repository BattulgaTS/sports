var Sequelize = require('sequelize');
var sequelize = new Sequelize ('csports', 'root', 'rooterino', {
	host: 'localhost',
	dialect : 'mysql',
	pool: {
		max: 5,
		min: 0,
		idle: 10000
	},
	logging: false
})

var models = [
  'Post', 					'Draft',
  'Team', 					'Game',
  'User', 					'Participant',
  'Match', 					'Prize',
	'Tournament', 		'Winner',
	'Stream'
];



module.exports = function() {
  if (global.db) return global.db

	global.db = {
		sequelize: sequelize,
		Sequelize: Sequelize
	}

	set_database_so_hard();

	return global.db;
}

var set_database_so_hard = function(){
	models.forEach(function(model) {
		if (!global.db[model]){
			global.db[model] = sequelize.import(__dirname + '/' + model);
			global.db[model].sync();
		}
	})

	set_relations()

};

var set_relations = function(){
	var Team = global.db.Team,
			Match = global.db.Match,
			Game = global.db.Game,
			Draft = global.db.Draft,
			Tournament = global.db.Tournament,
			Participant = global.db.Participant,
			Winner = global.db.Winner,
			Prize = global.db.Prize,
			Stream = global.db.Stream





			Tournament.hasMany(Participant, {as: 'participants', foreignKey: 'tournament_id'})
			Participant.belongsTo(Tournament, {as: 'tournament', foreignKey: 'tournament_id'})

			Tournament.hasMany(Participant, {as: 'qualifiedTeams', foreignKey: 'qualifiedT_id'})
			Participant.belongsTo(Tournament, {as: 'qualifiedT', foreignKey: 'qualifiedT_id'})

			Team.hasMany(Participant, {as: 'qualifiedTournaments', foreignKey: 'team_id'})
			Participant.belongsTo(Team, {as: 'team', foreignKey: 'team_id'})

	Tournament.hasMany(Winner, {as: 'winners', foreignKey: 'tournament_id'})
	Winner.belongsTo(Tournament, {as: 'tournament', foreignKey: 'tournament_id'})

	Team.hasMany(Winner, {as: 'winner', foreignKey: 'team_id'})
	Winner.belongsTo(Team, {as: 'team', foreignKey: 'team_id'})

			Team.hasMany(Match, {as: 'fMatches', foreignKey: 'fTeam_id'})
			Match.belongsTo(Team, {as: 'fTeam', foreignKey: 'fTeam_id'})

			Team.hasMany(Match, {as: 'sMatches', foreignKey: 'sTeam_id'})
			Match.belongsTo(Team, {as: 'sTeam', foreignKey: 'sTeam_id'})

			Tournament.hasMany(Match, {as: 'matches', foreignKey: 'tournament_id'})
			Match.belongsTo(Tournament, {as: 'tournament', foreignKey: 'tournament_id'})


	Match.hasMany(Stream, {as: 'streams', foreignKey: 'match_id'})
	Stream.belongsTo(Match, {as: 'match', foreignKey: 'match_id'})

			Match.hasMany(Game, {as: 'games', foreignKey: 'match_id'})
			Game.belongsTo(Match, {as: 'match', foreignKey: 'match_id'})

			Match.hasMany(Game, {as: 'game', foreignKey: 'winner_id'})
			Game.belongsTo(Team, {as: 'winner', foreignKey: 'winner_id'})

			Match.hasMany(Game, {as: 'game', foreignKey: 'loser_id'})
			Game.belongsTo(Team, {as: 'loser', foreignKey: 'loser_id'})

	Game.hasMany(Draft, {as: 'drafts', foreignKey: 'game_id'})
	Draft.belongsTo(Game, {as: 'game', foreignKey: 'game-_id'})

			Tournament.hasMany(Prize, {as: 'prizePools', foreignKey: 'tournament_id'})
			Prize.belongsTo(Tournament, {as: 'prizePools', foreignKey: 'tournament_id'})

}
