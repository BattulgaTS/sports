module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Stream', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    url: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    match_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
    // }, {
    //   instanceMethods: {
    //     countTasks: function() {
    //       // how to implement this method ?
    //     }
    //   }
  }, {
    timestamps: true
  });
};
