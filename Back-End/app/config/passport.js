// config/passport.js

// load all the things we need
var LocalStrategy = require('passport-local').Strategy;

// load up the user model

var bcrypt = require('bcrypt-nodejs');

module.exports = function(passport) {
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    global.db.User.find({
        attributes: ['id', 'username', 'password'],
        where: {
          id: id
        }
      })
      .then(function(res) {
        var user = res.dataValues;
        done(null, user);
      }).catch(function(err) {
        done(err)
      })

    // global.db.query("SELECT * FROM users WHERE uId = ?", [id], function(err, rows) {
    //   done(err, rows[0]);
    // });
  });

  passport.use(
    'local-signup',
    new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'username',
        passwordField: 'password'
      },
      function(username, password, done) {
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists

        global.db.User.find({
          attributes: ['id', 'username', 'password'],
          where: { username: username }
        })

        .catch(function(err){
          return done(err);
        })

        .then(function(user){
          if(user){
            console.log('User Already Exists!');
            done(null, false);
          }else{
            var newUser = {
              username: username,
              password: bcrypt.hashSync(password, null, null)
            };

            global.db.User.create(newUser)
            .then(function(user){
              newUser.id = user.insertId;
              return done(null, newUser);
            })
          }
        })
      })
  );
  passport.use(new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password'
    },
    function(username, password, done) { // callback with email and password from our form

      global.db.User.find({
        attributes: ['id', 'username', 'password'],
        where: {
          username: username
        }
      })

      .catch(function(err) {
        console.log(err);
        return done(err);
      })

      .then(function(user) {
        if (!user) return done(null, false)
        if (!bcrypt.compareSync(password, user.password)) return done(null, false);
        return done(null, user);
      })
    }));

};
