module.exports = function() {
    if (global.redisConfig) return global.redisConfig;

    var development = true

    if (development)
      return global.redisConfig = {
        host: '127.0.0.1'
      }
    else {
      return global.redisConfig = {
        host: '52.25.241.31'
      }
    }
}
