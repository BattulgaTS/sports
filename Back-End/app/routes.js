// Routes in Back-End

var express = require('express'),
  multer = require('multer')

module.exports = function(app, passport) {

  var storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, '../images/')
    },
    filename: function(req, file, cb) {
      cb(null, file.originalname)
    }
  })

  var upload = multer({
    storage: storage
  })

  app.use(express.static('../Front-End/dist'));
  app.use('/api/image', express.static('../images'));
  var routes = {
    admin: [
      '/user/edit',
      'user/all'
    ],
    user: [
      '/post/all',
      'post/get'
    ]
  }

  app
    .use('/api/post/', require('./handlers/post/post'))
    .use('/api/allposts', require('./handlers/post/posts'))
    .use('/api/newpost', upload.single('pic'), require('./handlers/post/new'))
    .use('/api/category/', require('./handlers/post/category'))
    .use('/api/topposts', require('./handlers/post/top'))

    .use('/api/newteam', upload.single('logo'), require('./handlers/team/new'))
    .use('/api/get/teams', require('./handlers/team/teams'))
    .use('/api/get/team/', require('./handlers/team/team'))

    .use('/api/new/match', upload.single('pic'), require('./handlers/match/new'))
    .use('/api/get/matches', require('./handlers/match/matches'))
    .use('/api/get/match/', require('./handlers/match/match'))


    .use('/api/user/status', function(req, res) {
      if (req.isAuthenticated()) {
        console.log('User with %s username is authenticated', req.user.username)
        res.status(200).send(true);
      } else {
        console.log('Authenticated user not found!')
        res.status(401).send(false);
      }
      res.end();
    })
    .post('/api/login', function(req, res, next) {
      passport.authenticate('local', function(err, user, info) {

        if (err) {

          console.log('auth err', err);

          return next(err);
        }

        if (user) {

          req.logIn(user, function(err) {

            if (err) return next(err)

            console.log('Found user: ', user);

            res.send(true);
          })

        } else {

          console.log('User not found!');

          res.status(401).send('User not found!');
        }

      })(req, res, next);
    })

  .post('/api/register', function(req, res, next) {
      passport.authenticate('local-signup', function(err, user, info) {
        if (err) {
          return next(err);
        }
        res.send('Registered!');
      })(req, res, next);
    })
    .get('/api/logout', function(req, res) {
      req.logout();
      res.end();
    })
}




var isLogged = function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.send(false);
  }
  res.end();
};
