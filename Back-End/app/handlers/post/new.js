module.exports = function (req, res){
  console.log('new POST request in /newpost');
  console.log("request file is: ",req.file);
  console.log("request body is: ",req.body);
  if (req.body.Top == 'true') console.log('This one is TOP POST!');
  enterPost(req, res);
}

var enterPost = function(req, res){

  global.db.
    Post
      .create({
        topic: req.body.Topic,
        category: req.body.Category,
        content: req.body.Content,
        top: req.body.Top == 'true' ? true : false,
        picture: req.file ? req.file.originalname : null
      })
      .error(function(err){
        console.log(err);
      })
      .then(function(){
        console.log('Created Successfully!');
        res.send('Success!');
      })

}
