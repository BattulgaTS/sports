module.exports = function(req, res){
  console.log("Requesting Single post with %d id", req.url.substring(1))

  global.db.Post.find({
    attributes: {
      exclude: ['createdAt', 'updatedAt']
    },
    where: {
      id: req.url.substring(1)
    }
  })

  .catch(function(err){
    console.log(err);
  })

  .then(function(response){
    giff(res, response.dataValues);
  })
}

var giff = function(res, data){
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
  console.log("Returning Team: ", data);
  res.end();
}
