module.exports = function(req, res){

  global.db.Post.findAll({
    attributes: {
      exclude: ['createdAt', 'updatedAt']
    },
    order: '"date" DESC'
  })

  .catch(function(err){
    console.log(err);
  })

  .then(function(response){
    var giving_back = [];
    response.forEach(function(inst){
      giving_back.push(inst.get());
    })
    giff(res, giving_back);
  })

}

var giff = function(res, data){
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
  console.log("Posts handler returned %d data", data.length);
  res.end();
}
