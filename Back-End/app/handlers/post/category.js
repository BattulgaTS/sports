module.exports = function(req, res){
  console.log('Requested ' + req.url.substr(1) +' Category');

  global.db.Post.findAll({
    attributes: {
      exclude: ['createdAt', 'updatedAt']
    },
    order: '"date" DESC',
    where: {
      category: req.url.substr(1)
    }
  })

  .catch(function(err){
    console.log(err);
  })

  .then(function(response){
    var giving_back = [];
    response.forEach(function(inst){
      giving_back.push(inst.get());
    })
    giff(res, giving_back);
  })
}

var giff = function(res, data){
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
  console.log("Category's total posts: ", data.length);
  res.end();
}
