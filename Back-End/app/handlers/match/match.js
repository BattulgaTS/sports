module.exports = function(req, res){
  var id = req.url.substr(1);

  global.db.Match.find({
    attributes: {
      exclude: ['createdAt', 'updatedAt']
    },
    where: {
      id: id
    }
  })

  .catch(function(err){
    console.log(err);
  })

  .then(function(response){
    giff(res, response.get());
  })
}

var giff = function(res, data){
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
  console.log("/match replied with ", data);
  res.end();
}
