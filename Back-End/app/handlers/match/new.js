module.exports = function (req, res){
  console.log('new POST request in /new/match');
  console.log("request body is: ",req.body);
  console.log("date is: ",req.body.date.substr(4, 20));
  enter(req, res);
}

var enter = function(req, res){
  global.db.
    Post
      .create({
        team1: req.body.team1,
        team2: req.body.team2,
        date: new Date(req.body.date),
        bestOf: req.body.bestOf

      })
      .error(function(err){
        console.log(err);
      })
      .then(function(){
        console.log('Created Successfully!');
      })

  res.send('Created Successfully!');
  res.end();
}
