
module.exports = function(req, res, next, passport){
  console.log('Auth: ',req.isAuthenticated());
  if (req.isAuthenticated()) return next();
  res.redirect('/admin/login');
}
