var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

module.exports = function (req, res){
  console.log('new POST request in /newteam');
  console.log("request file is: ",req.file);
  console.log("request body is: ",req.body);
  enterPost(req, res);
}

var enterPost = function(req, res){

  global.db.
    Team
      .create({
        title: req.body.title,
        region: req.body.region,
        description: req.body.description,
        logo: req.file ? req.file.originalname : null,
        owner: req.body.owner,
        sponsor: req.body.sponsor
      })

      .then(function(){
        console.log('Created Successfully!');
        res.send('Created Successfully!');
      })

      .catch(function(err){
        console.log(err);
      })


}
