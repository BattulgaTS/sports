module.exports = function(req, res){

  global.db.Team.findAll({
    attributes: {
      exclude: ['createdAt', 'updatedAt']
    },
    order: '"id" ASC'
  })

  .then(function(response){
    var giving_back = [];
    response.forEach(function(inst){
      giving_back.push(inst.get());
    })
    giff(res, giving_back);
  })

  .catch(function(err){
    console.log(err);
  })


}

var giff = function(res, data){
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
  console.log(data);
  console.log("Successfully returned %d teams", data.length);
}
