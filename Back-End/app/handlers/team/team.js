module.exports = function(req, res){
  var id = req.url.substr(1);
  console.log('requested team id = ', req.url.substr(1))

  global.db.Post.find({
    attributes: {
      exclude: ['createdAt', 'updatedAt']
    },
    where: {
      id: req.url.substring(1)
    }
  })

  .catch(function(err){
    console.log(err);
  })

  .then(function(response){
    giff(res, response.get());
  })

}

var giff = function(res, data){
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
  console.log("Returned Team: ", data);
  res.end();
}
