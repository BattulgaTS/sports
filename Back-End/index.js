// App Modules

var express = require('express'),
  bodyParser = require('body-parser'),
  multipart = require('connect-multiparty'),
  passport = require('passport'),
  session = require('express-session'),
  cookie = require('cookie-parser'),
  multer = require('multer'),
  RedisStore = require('connect-redis')(session),
  Sequelize = require('sequelize'),
  log4js = require('log4js')


require('./app/config/models')();
require('./app/config/passport')(passport);
require('./app/config/node_env')();

var logger = log4js.getLogger();
var app = express()
var multipartMiddleware = multipart();

// global.db.Post.destroy({
//   where: {
//     id: {
//       $ne : 20
//     }
//   }
// })

app.use(log4js.connectLogger(logger, { level: log4js.levels.DEBUG, format: ':method :url :status' }));

// logger.trace('Entering cheese testing');
// logger.debug('Got cheese.');
// logger.info('Cheese is Gouda.');
// logger.warn('Cheese is quite smelly.');
// logger.error('Cheese is too ripe!');
// logger.fatal('Cheese was breeding ground for listeria.');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({
  extended: true
})); // for parsing application/x-www-form-urlencoded
app.use(cookie());

app.use(session({
  secret: 'TopSecretKey',
  resave: true,
  saveUninitialized: true,
  cookie: {
    maxAge: 60 * 60 * 1000
  },
  store: new RedisStore(global.redisConfig)
}));
app.use(passport.initialize());
app.use(passport.session());

//Routing App
app.use(require('./let.enter.js'));

var a = function(){

  global.db.Team.findAll({
    include: [
      {
        model: global.db.Match,
        as: 'sMatches',
      },
      {
        model: global.db.Match,
        as: 'fMatches'
      }
    ]
  })
  .catch(function(err){
    console.log(err);
  })

  .then(function(data){
    console.log(JSON.stringify(data, false, 4) );
  })

}();



require('./app/routes')(app, passport);


app.listen(8080)
console.log('Magic is happening on port 8080')
