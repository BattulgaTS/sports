module.exports = function (req, res, next) {
    // res.header("Access-Control-Allow-Origin", req.headers.origin);
    // res.header("Access-Control-Allow-Credentials", true);
    // res.header("Access-Control-Allow-Headers", true);

    if (req.headers.origin === "http://localhost:3000") res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
    else {
          res.setHeader("Access-Control-Allow-Origin", "http://54.200.145.18");
    }

    // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000", 'http://54.200.145.18');
    // res.setHeader("Access-Control-Allow-Origin", req.headers.origin);
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader("Access-Control-Allow-Methods", "POST, GET");
    res.setHeader("Access-Control-Max-Age", "3600");
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    next();
}
